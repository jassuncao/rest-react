# Implementando [REST](https://pt-br.reactjs.org/tutorial/tutorial.html#inspecting-the-starter-code) em [React](<[https://medium.com/code-prestige/como-criar-um-app-react-consumindo-um-back-end-node-com-express-5030e1727ace](https://medium.com/code-prestige/como-criar-um-app-react-consumindo-um-back-end-node-com-express-5030e1727ace)>)!

[![node](https://img.shields.io/badge/node-8.10.0-green.svg)](<[https://nodejs.org/en/download/](https://nodejs.org/en/download/)>)
[![yarn](https://img.shields.io/badge/yarn-1.16.0-blue.svg)](<[https://yarnpkg.com/pt-BR/docs/install#debian-stable](https://yarnpkg.com/pt-BR/docs/install#debian-stable)>)
[![create-react-app](https://img.shields.io/badge/create--react--app-3.0.0-orange.svg)](<[https://tableless.com.br/criando-sua-aplicacao-react-em-2-minutos/](https://tableless.com.br/criando-sua-aplicacao-react-em-2-minutos/)>)
[![node-fetch](https://img.shields.io/badge/node--fetch-2.6.0-red.svg)](<[https://www.npmjs.com/package/node-fetch](https://www.npmjs.com/package/node-fetch)>)
[![bootstrap](https://img.shields.io/badge/bootstrap-4.1.3-blueviolet.svg)](<[https://getbootstrap.com/](https://getbootstrap.com/)>)

Esse é um código para consumir dados de uma API REST usando o react.
Para esse exemplo, foi usado a api pública de consulta de CEP da [viaCEP](https://viacep.com.br/).
Documentação da API em swagger: [CEP](https://jassuncao.gitlab.io/rest-react)

### Para baixar dependências:

    yarn install

# Tutorial
## Projeto final: 
![Sucesso](https://gitlab.com/jassuncao/rest-react/raw/master/img/Sucesso.png)<br/>
![Não Encontrado](https://gitlab.com/jassuncao/rest-react/raw/master/img/nao-encontrado.png)<br/>
![Erro](https://gitlab.com/jassuncao/rest-react/raw/master/img/Error.png)


### Estrutura das pastas:

![Estrutura Projeto](https://gitlab.com/jassuncao/rest-react/raw/master/img/EstruturaPastas.png)
O arquivo que será modificado é o `App.js`

Essa é a estrutura básica do `App.js`:

```js
1 |	import  React  from  "react"; //Importar a classe do React para renderizar
2 |	const  fetch  =  require("node-fetch"); // Importa o framework para realizar API
3 |
4 |	class  App  extends  React.Component {
5 |
6 |	// Define o valor inicial, quando a aplicação é carregada...
7 |	state  = {
8 |		cep: [] // Quando a aplicação for carregada,o CEP é zerado
9 |	};
10|
11|		// Armazena a referência do input
12|		handleInputRef  = (input) => {
13|			/* TODO */
14|		};
15|
16|		// Realiza a requisição REST do cep ao servidor do viaCEP.
17|		handleSend  = () => {
18|			/* TODO */
19|		}
20|
21|		//Função que exibe uma mensagem dependendo do retorno da API
22|		returnCEP() {
23|			/* TODO */
24|		}
25|
26|	// Define a formatação do HTML para renderizar no cliente
27|	render() {
28|		return (
29|			/* TODO*/
30|		);
31|	}
32|
33|	export  default  App;
```

<br />

Agora tem que implementar as funções:

```js
11|		// Armazena a referência do input
12|		handleInputRef  = (input) => {
13|			/* TODO */
14|		};
15|
16|		// Realiza a requisição REST do cep ao servidor do viaCEP.
17|		handleSend  = () => {
18|			/* TODO */
19|		}
20|
21|		//Função que exibe uma mensagem dependendo do retorno da API
22|		returnCEP() {
23|			/* TODO */
24|		}
25|
26|		// Define a formatação do HTML para renderizar no cliente
27|		render() {
28|			return (
29|				/* TODO*/
30|			);
31|		}
```

<br />

A função `handleInputRef` irá armazenar o valor digitado no input:

```js
11|		// Armazena a referência do input
12|		handleInputRef  = (input) => {
13|			this.input  =  input;
14|		};
```

<br />

A função `handleSend` realiza a chamada na API REST da [viaCEP](https://viacep.com.br/):

```js
16|	// Realiza a requisição REST do cep ao servidor do viaCEP.
17|	handleSend  = () => {
18|
19|		// Passa na url qual o CEP que foi informado no input
20|		fetch(`https://viacep.com.br/ws/${this.input.value}/json/`)
21|		.then((res) =>  res.json()) // Converte para JSON
22|		.then((data) => { // Caso a consulta tenha sido com sucesso.
23|
24|			// Verifica se o CEP foi localizado.
25|			if (!!data.cep) this.setState({ cep:  data });
26|			else  this.setState({ cep:  undefined });
27|		})
28|		.catch(() => {
29|
30|			//Caso tenha dado algum erro
31|			this.setState({ cep:  null });
32|		});
33|	};
```

<br />

A função `returnCEP` define qual a mensagem([Sucesso](https://gitlab.com/jassuncao/rest-react/raw/master/img/Sucesso.png), [Não Encontrado](https://gitlab.com/jassuncao/rest-react/raw/master/img/nao-encontrado.png), [Erro](https://gitlab.com/jassuncao/rest-react/raw/master/img/Error.png)) e os dados que serão renderizados.

```js
21|	//Função que exibe uma mensagem dependendo do retorno da API
22|	returnCEP() {
23|
24|		// Variavel que armazena a mensagem a ser exibida
25|		var  result  =  null;
26|
27|		//Verifica o retorno do servidor
28|		switch (this.state.cep) {
29|
30|			// Caso seja undefined, então não foi encontrado o CEP
31|			case  undefined:
32|				result  = (
33|					<div  class="alert alert-warning"  role="alert">
34|						CEP não encontrado!
35|					</div>
36|				);
37|			break;
38|
39|			// Caso seja null, então foi passado alguma informação que não é um CEP
40|			case  null:
41|				result  = (
42|					<div  class="alert alert-danger"  role="alert">
43|						Digite os 8 números do CEP
44|					</div>
45|				);
46|			break;
47|
48|			// Caso o servidor retorne uma mensagem de sucesso
49|			default:
50|				// Verifica se o status do CEP existe
51|				if (!!this.state.cep.cep) {
52|					result  = (
53|						<div  class="alert alert-success"  role="alert">
54|							CEP: <b>{this.state.cep["cep"]}</b>
55|							<br  />
56|							Rua: <b>{this.state.cep["logradouro"]}</b>
57|							<br  />
58|							Complemento: <b>{this.state.cep["complemento"]}</b>
59|							<br  />
60|							Bairro: <b>{this.state.cep["bairro"]}</b>
61|							<br  />
62|							Cidade: <b>{this.state.cep["localidade"]}</b>
63|							<br  />
64|							Estado: <b>{this.state.cep["uf"]}</b>
65|							<br  />
66|							Cod. IBGE: <b>{this.state.cep["ibge"]}</b>
67|							<br  />
68|						</div>
69|					);
70|				}
71|			break;
72|		}
73|
74|		return  result; // Retorna a mensagem
75|	}
```

<br />

A função `render` renderiza o HTML a ser exibido para o cliente:

```js
26|	// Define a formatação do HTML para renderizar no cliente
27|	render() {
28|		return (
29|			<div  className="container">
30|				<div  className="col-xs-12">
31|					<h1>Buscar CEP</h1>
32|					<div  class="input-group mb-3">
33|						<input type="text" class="form-control" placeholder="Digite um CEP"
34|						  ref={this.handleInputRef}/>
35|						<div  class="input-group-append">
36|							<button onClick={this.handleSend} type="button"
37|							   class="btn btn-primary">
38|								Consultar
39|							</button>
40|						</div>
41|					</div>
42|					{this.returnCEP()}
43|				</div>
44|			</div>
45|		);
46|	}
```

Código Final: [App.js](https://gitlab.com/jassuncao/rest-react/blob/master/src/App.js)

### Para executar:

    yarn start

URL: [http://localhost:3000/](http://localhost:3000/)

## Para gerar build:

    yarn build


## [Documentação swagger](https://gitlab.com/jassuncao/rest-react/blob/master/docs/doc-swagger.yaml)

```yaml
swagger: '2.0'
info:
  description: Documentação da API viaCEP
  title: viaCEP
  version: v1
schemes:
  - https
host: "viacep.com.br"  
paths:
  /ws/{cep}/json/:
    get:
      summary: Get CEP
      parameters:
      - name: "cep"
        in: "path"
        description: "cep"
        required: true
        type: "string"
      responses:
        '200':
          description: Success response
          schema:
            type: object
            properties:
                cep: 
                  type: string
                  example: 01001-000
                logradouro: 
                  type: string
                  example: Praça da Sé
                complemento: 
                  type: string
                  example: lado ímpar
                bairro: 
                  type: string
                  example: Sé
                localidade: 
                  type: string
                  example: São Paulo
                uf: 
                  type: string
                  example: SP
                unidade: 
                  type: string
                  example: ''
                ibge: 
                  type: string
                  example: '3550308'
                gia: 
                  type: string
                  example: '1004'
                
```
### Aluno: Jônathas Assunção Alves - 201613279
