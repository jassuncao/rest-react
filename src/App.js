import React from "react";

// Importa o framework para realizar API
const fetch = require("node-fetch");

class App extends React.Component {
  
  // Define o valor inicial, quando a aplicação é carregada... 
  state = {
    cep: [] // Quando a aplicação for carregada,o CEP é zerado
  };

  // Armazena a referência do input
  handleInputRef = (input) => {
    this.input = input;
  };

  // Realiza a requisição REST do cep ao servidor do viaCEP.
  handleSend = () => {
    // Passa na url qual o CEP que foi informado no input
    fetch(`https://viacep.com.br/ws/${this.input.value}/json/`)
      .then((res) => res.json()) // Converte para JSON
      .then((data) => { // Caso a consulta tenha sido com sucesso.

        //  Verifica se o CEP foi localizado.
        if (!!data.cep) this.setState({ cep: data }); 
        else this.setState({ cep: undefined });
      })
      .catch(() => {
        //Caso tenha dado algum erro
        this.setState({ cep: null });
      });
  };

  //Função que exibe uma mensagem dependendo do retorno da API
  returnCEP() {
    // Variavel que armazena a mensagem a ser exibida
    var result = null;

    //Verifica o retorno do servidor
    switch (this.state.cep) {

      // Caso seja undefined, então não foi encontrado o CEP
      case undefined:
        result = (
          <div class="alert alert-warning" role="alert">
            CEP não encontrado!
          </div>
        );
        break;

      // Caso seja null, então foi passado alguma informação que não é um CEP
      case null:
        result = (
          <div class="alert alert-danger" role="alert">
            Digite os 8 números do CEP
          </div>
        );
        break;

      // Caso o servidor retorne uma mensagem de sucesso 
      default:

        // Verifica se o status do CEP existe
        if (!!this.state.cep.cep) {
          result = (
            <div class="alert alert-success" role="alert">
              CEP: <b>{this.state.cep["cep"]}</b>
              <br />
              Rua: <b>{this.state.cep["logradouro"]}</b>
              <br />
              Complemento: <b>{this.state.cep["complemento"]}</b>
              <br />
              Bairro: <b>{this.state.cep["bairro"]}</b>
              <br />
              Cidade: <b>{this.state.cep["localidade"]}</b>
              <br />
              Estado: <b>{this.state.cep["uf"]}</b>
              <br />
              Cod. IBGE: <b>{this.state.cep["ibge"]}</b>
              <br />
            </div>
          );
        }
        break;
    }
    return result; // Retorna a mensagem
  }

  // Define a formatação do HTML para renderizar no cliente
  render() {
    return (
      <div className="container">
        <div className="col-xs-12">
          <h1>Buscar CEP</h1>
          <div class="input-group mb-3">
            <input
              type="text"
              class="form-control"
              placeholder="Digite um CEP"
              aria-describedby="basic-addon2"
              ref={this.handleInputRef}
            />
            <div class="input-group-append">
              <button
                onClick={this.handleSend}
                type="button"
                class="btn btn-primary"
              >
                Consultar
              </button>
            </div>
          </div>
          {this.returnCEP()}
        </div>
      </div>
    );
  }
}
export default App;
